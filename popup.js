let timeReportPanel = document.getElementById("timeReportPanel");
let setApiKeyPanel = document.getElementById("setApiKeyPanel");
let loadingPanel = document.getElementById("loadingPanel");
let projectSelect = document.getElementById("projectSelect");
let taskSelect = document.getElementById("taskSelect");
let timeInput = document.getElementById("timeInput");
let timeEntityNoteInput = document.getElementById("timeEntityNoteInput");
let apiKeyInput = document.getElementById("apiKeyInput");
let accountIdInput = document.getElementById("accountIdInput");
let getApiKeyButton = document.getElementById("getApiKeyButton");
let setApiKeyButton = document.getElementById("setApiKeyButton");
let changeApiKeyButton = document.getElementById("changeApiKeyButton");
let saveButton = document.getElementById("saveButton");
let cancelButton = document.getElementById("cancelButton");

let apiUrl = 'https://api.harvestapp.com/v2';

var project_assignments; 
var apiCredentials;

function setTasksForProject(project_id) {
    let optionCount = taskSelect.length;
    for (i = 0; i < optionCount; i++)
        taskSelect.remove(0);

    for (i = 0; i < project_assignments.length; i++) {
        var project_assignment = project_assignments[i];
        if (project_assignment.project.id == project_id) {
            project_assignment.task_assignments.forEach(task_assignment => {
                let option = document.createElement('option');
                option.value = task_assignment.task.id;
                option.text = task_assignment.task.name;
                taskSelect.appendChild(option);
            });

            break;
        }
    }
}

function getStoredCredentials() {
    return new Promise((resolve, reject) => {
        chrome.storage.sync.get("pat", (pat_response) => {
            if (!chrome.runtime.lastError) {
                chrome.storage.sync.get("account_id", (account_id_response) => {
                    if (!chrome.runtime.lastError) {
                        var credentials = { 
                            'pat': pat_response.pat, 
                            'account_id': account_id_response.account_id
                        };

                        resolve(credentials);
                    }
                    else
                        reject(chrome.runtime.lastError);
                });
            }
            else {
                reject(chrome.runtime.lastError);
            }
        });
    });
}

function goToSetApiKey() {
    timeReportPanel.style.display = "none";
    setApiKeyPanel.style.display = "block";
    loadingPanel.style.display = "none";

    chrome.tabs.query({ active: true, currentWindow: true }, function(tabs) {
        chrome.scripting.executeScript({
            target: { tabId: tabs[0].id },
            function: function() {
                return document.getElementById("client_secret").value;
            }
        },
        (injectionResult) => {
            if (!injectionResult || injectionResult.length == 0)
                return;

            apiKeyInput.value = injectionResult[0].result;
        });

        chrome.scripting.executeScript({
            target: { tabId: tabs[0].id },
            function: function() {
                return document.getElementById("playground_account").value;
            }
        },
        (injectionResult) => {
            if (!injectionResult || injectionResult.length == 0)
                return;

            accountIdInput.value = injectionResult[0].result;
        });
    });
}

function goToTimeReport() {
    timeReportPanel.style.display = "block";
    setApiKeyPanel.style.display = "none";
    loadingPanel.style.display = "none";
}

function goToLoading() {
    timeReportPanel.style.display = "none";
    setApiKeyPanel.style.display = "none";
    loadingPanel.style.display = "block";
}

function getApiRequestHeaders() {
    return {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${apiCredentials.pat}`,
        'Harvest-Account-Id': apiCredentials.account_id
    };
}

function fetchProjectAssignments() {
    getStoredCredentials()
        .then(credentials => {
            apiCredentials = credentials;

            fetch(`${apiUrl}/users/me/project_assignments`, {
                headers: getApiRequestHeaders(),
                mode: 'cors'
            })
            .then(response => response.json())
            .then(result => {
                project_assignments = result.project_assignments;
                project_assignments.forEach(project_assignment => {
                    let option = document.createElement('option');
                    option.value = project_assignment.project.id;
                    option.text = project_assignment.project.name;
                    projectSelect.appendChild(option);
                });

                if (project_assignments.length > 0) {
                    setTasksForProject(project_assignments[0].project.id);
                }

                goToTimeReport();
            })
            .catch(error => {
                console.log('Fetch project assignments error: ' + error);
                goToSetApiKey();
            });
        })
        .catch(error => {
            console.log(`Get stored API key error: ${error}`);
            goToSetApiKey();
        });
}

function sendTimeEntity(project_id, task_id, hours, notes) {
    var spent_date = new Date().toISOString().substring(0, 10);

    var urlParams = new URLSearchParams({
        "project_id": project_id,
        "task_id": task_id,
        "hours": hours,
        "spent_date": spent_date,
        "notes": notes
    });

    fetch(`${apiUrl}/time_entries?` + urlParams, {
        method: 'POST',
        headers: getApiRequestHeaders()
    })
    .then(response => response.json())
    .then(result => {
        timeInput.value = '';
        timeEntityNoteInput.value = '';
    })
    .catch(error => {
        console.log(`Send time entity error: ${error}`);
    });
}

goToLoading();
fetchProjectAssignments();

projectSelect.addEventListener('change', (event) => {
    setTasksForProject(event.target.value);
});

getApiKeyButton.addEventListener('click', async () => {
    chrome.tabs.update({
        url: "https://id.getharvest.com/developers"
    });
});

setApiKeyButton.addEventListener('click', async () => {
    if (apiKeyInput.value.length == 0 || accountIdInput.value.length == 0)
        return;

    chrome.storage.sync.set({ pat: apiKeyInput.value });
    chrome.storage.sync.set({ account_id: accountIdInput.value });

    goToLoading();
    fetchProjectAssignments();
});

changeApiKeyButton.addEventListener('click', async () => {
    goToSetApiKey();
});

saveButton.addEventListener("click", async () => {
    var project_id = projectSelect.value;
    var task_id = taskSelect.value;
    var hours = Number.parseFloat(timeInput.value);
    var notes =  timeEntityNoteInput.value;

    if (hours == NaN) {
        console.log(`Incorrect data: project_id = ${project_id}; task_id = ${task_id}; hours = ${hours}`);
        return;
    }

    sendTimeEntity(project_id, task_id, hours, notes);
});

cancelButton.addEventListener("click", async () => {
    window.close();
});